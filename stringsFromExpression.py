def findClosingBracket(string, indexToStartFrom):
    counter = 1
    currentIndex = indexToStartFrom
    while True:
        if string[currentIndex] == '(':
            counter += 1
        elif string[currentIndex] == ')':
            counter -= 1
            if counter == 0:
                return currentIndex
        currentIndex += 1

def multiplyStringsSets(setA, setB):
    if setA == []:
        setA = ['']
    if setB == []:
        setB = ['']
    result = []
    for stringA in setA:
        for stringB in setB:
            result.append(stringA + stringB)
    return result

def isList(something):
    return type(something) == type([])

def stringsFromExpression(expression, logging = False):
    if logging:
        print('processing expression', expression)
    strings = []
    expressionLength = len(expression)
    currentOperator = '|'
    currentOperand = None
    i = 0
    while True:
        if logging:
            print('found', expression[i], 'on index', i)
        if expression[i] == '(':
            closingBracketIndex = findClosingBracket(expression, i + 1)
            if logging:
                print('closing bracket is on index', closingBracketIndex)
            stringInBrackets = expression[i + 1 : closingBracketIndex]
            if logging:
                print('string in brackets is', stringInBrackets)
            currentOperand = stringsFromExpression(stringInBrackets)
            i = closingBracketIndex
        elif (expression[i] == '|') or (expression[i] == '*'):
            currentOperator = expression[i]
            currentOperand = None
        else:
            currentOperand = expression[i]
        if currentOperand:
            if logging:
                print('current operand is', currentOperand, 'current operator is', currentOperator)
                print('strings before operation:', strings)
            if currentOperator == '|':
                if isList(currentOperand):
                    strings += currentOperand
                else:
                    strings.append(currentOperand)
            elif currentOperator == '*':
                strings = multiplyStringsSets(strings.copy(), currentOperand)
            if logging:
                print('strings after operation:', strings)
        i += 1
        if i == expressionLength:
            break
    if logging:
        print('end processing expression', expression, ', got strings', strings)
    return strings
        
        

expression = 'a*b*c*(a|b|l|o*(O|X)|i)*d*(r|k|e)*r'
strings = stringsFromExpression(expression)
print(strings)
