import math
import numpy

def TableManipPZK(x,y,z,a,b,j):    
    def m01(a):   
        m011=numpy.matrix([[math.cos(a), -math.sin(a),  0, 0],
                           [math.sin(a),  math.cos(a),  0, 0],
                            [         0,            0,  1, 0],
                            [         0,            0,  0, 1]])
        
        m012=numpy.matrix([[1, 0, 0,  0],
                           [0, 1, 0,  0],
                           [0, 0, 1, -90],
                           [0, 0, 0,  1]])
                       
                          
        return m011*m012
    def m12(b):   
        m121=numpy.matrix([[ math.cos(b), 0, math.sin(b), 0],
                           [           0, 1,           0, 0],
                           [-math.sin(b), 0, math.cos(b), 0],
                           [           0, 0,           0, 1]])
        
        m122=numpy.matrix([[1, 0, 0,  0],
                           [0, 1, 0,  0],
                           [0, 0, 1, -41],
                           [0, 0, 0,  1]])
                       
                       
        return m121*m122    
    def m23(j):   
        m231=numpy.matrix([[ math.cos(j), 0, math.sin(j), 0],
                           [           0, 1,           0, 0],
                           [-math.sin(j), 0, math.cos(j), 0],
                           [           0, 0,           0, 1]])
        
        m232=numpy.matrix([[1, 0, 0,  0],
                           [0, 1, 0,  0],
                           [0, 0, 1, -67],
                           [0, 0, 0,  1]])
                       
                       
        return m231*m232
    #a=math.pi/4
    #b=math.pi/4
    #j=-math.pi/4
    p0=numpy.matrix([[0], [0], [0], [1]])
    p1=numpy.matrix([[0], [0], [90], [1]])
    p21=numpy.matrix([[0], [0], [41], [1]])
    p31=numpy.matrix([[0], [0], [67], [1]])
    p41=numpy.matrix([[0], [0], [26], [1]])
    p2=m01(-a).getI()*p21
    p3=m01(-a).getI()*m12(-b).getI()*p31
    p4=m01(-a).getI()*m12(-b).getI()*m23(-j).getI()*p41
    p= numpy.concatenate([p1,p2,p3,p4],axis=1)
    l=((x-p4[0,0])**2+(y-p4[1,0])**2+(z-p4[2,0])**2)**0.5;
    return l