import cv2 
import numpy as np 

# Read image. 
gray = cv2.imread("sphere.jpg",0)
#cv2.imshow("gray", gray)

# Convert to grayscale. 
#gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) 

# Blur using 3 * 3 kernel. 
gray_blurred = cv2.blur(gray, (3, 3)) 
#cv2.imshow("gray blurred", gray_blurred)

# Apply Hough transform on the blurred image. 
detected_circles = cv2.HoughCircles(gray_blurred, cv2.HOUGH_GRADIENT, 1, 20, param1 = 140, param2 = 30, minRadius = 10, maxRadius = 125) 

# Draw circles that are detected. 
if detected_circles is not None: 

	# Convert the circle parameters a, b and r to integers. 
	detected_circles = np.uint16(np.around(detected_circles)) 

	for pt in detected_circles[0, :]: 
		a, b, r = pt[0], pt[1], pt[2] 

		# Draw the circumference of the circle. 
		cv2.circle(gray, (a, b), r, (0, 255, 0), 2)

		# Draw a small circle (of radius 1) to show the center. 
		cv2.circle(gray, (a, b), 1, (0, 0, 255), 3) 
else:
    print("there is no circles")

cv2.imshow("Detected Circles", gray) 
cv2.waitKey(0)
