#!/usr/bin/env python

import copy
import json
import math
import string
import sys
import time

import numpy as np

import rospy
from geometry_msgs.msg import Point
from visualization_msgs.msg import MarkerArray,Marker

def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    By https://stackoverflow.com/questions/6802577/rotation-of-3d-vector
    """
    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


class Element(object):
    def __init__(self):
        pass

    def trace_param(self, point):
        assert(False)
    def point(self, param):
        assert(False)
    def direction(self, param):
        assert(False)
    
    def error(self, point):
        ''' calculate distance from element to point
        with respect to projection value (projection should be positive);
        return distance and param (on trajectory it is in [0,1])'''
        param = self.trace_param(point)
        return (np.linalg.norm(self.point(param) - point), param)

    def next_point(self, curr_point):
        ''' calculate endpoint of trajectory
        if current element will be added to it'''
        return curr_point + self.point(1)

    def next_direction(self, curr_point):
        return self.direction(1)

    def get_vis_points(self, curr_point, num_points = 5):
        ''' return point positions for visualization '''
        return [curr_point + self.point(v)
                for v in np.linspace(1.0 / num_points, 1, num=num_points)]

    
class Straight(Element):
    def __init__(self, dr):
        self.dr = dr
        
    def __str__(self):
        ''' print description '''
        return 'Line: ({0:.3f}, {1:.3f}, {2:.3f})'.format(self.dr[0], self.dr[1], self.dr[2])

    def _length(self):
        ''' length of element '''
        return np.linalg.norm(self.dr)

    def direction(self, param):
        ''' direction of the trajectory element 
        parameterized by [0,1] parameter value:
        param==0 in initial point of the element,
        param==1 in end point of the element '''
        return self.dr / self._length()
    
    def point(self, param):
        ''' calculate point on element by parameter [0,1],
        where point[0] is starting point, point[1] is end point '''
        return self.dr * param

    def trace_param(self, point):
        ''' calculate parameter that matches 
        the nearest point on the trajectory to 'point'
        parameter. If it is in [0,1] interval 
        then this nearest point is on the element '''
        return np.dot(self.dr, point) / self._length()**2
    

class Arc(Element):
    def __init__(self, direction, center, angle):
        self.direction = direction / np.linalg.norm(direction)
        self.center = center
        self.angle = angle
        # axis of rotation
        self.axis = np.cross(self.direction, self.center)
        self.axis = self.axis / np.linalg.norm(self.axis)
        
    def _rotation_matrix(self, angle_deg):
        ''' rotation matrix for angle=angle_deg '''
        return rotation_matrix(self.axis, angle_deg * math.pi / 180.0)
        
    def __str__(self):
        templ = 'Arc: center({0:.3f}, {1:.3f}, {2:.3f}), dir({3:.3f}, {4:.3f}, {5:.3f}), angle {6:.3f}'
        return templ.format(self.center[0], self.center[1], self.center[2],
                            self.direction[0], self.direction[1], self.direction[2],
                            self.angle)
    
    def direction(self, param):
        ''' direction of the trajectory element 
        parameterized by [0,1] parameter value:
        param==0 in initial point of the element,
        param==1 in end point of the element '''
        rm = self._rotation_matrix(param * self.angle)
        return np.dot(rm, self.direction)
    
    def point(self, param):
        ''' calculate point on element by parameter [0,1],
        where point[0] is starting point, point[1] is end point '''
        rm = self._rotation_matrix(param * self.angle)
        #print(self.center)
        #print(np.dot(rm, self.center))
        return self.center - np.dot(rm, self.center)

    def trace_param(self, point):
        ''' calculate parameter that matches 
        the nearest point on the trajectory to 'point'
        parameter. If it is in [0,1] interval 
        then this nearest point is on the element '''
        if np.isclose(point, self.center).all():
            return -1  # one definite value is not calculable
        #print(self.axis)
        coeff_ax = np.dot(self.axis, point)
        #print(coeff_ax)
        pl_point = point - coeff_ax * self.axis
        #print(pl_point)
        coeff_dir = np.dot(self.direction, pl_point)
        #print(coeff_dir)
        rd_point = pl_point - coeff_dir*self.direction
        #print(rd_point)
        radius = np.linalg.norm(self.center)
        coeff_rad = np.dot(self.center, rd_point) / radius
        #print(coeff_rad)
        diff_n = radius - coeff_rad
        diff_t = coeff_dir
        return 180.0 * math.atan2(diff_t, diff_n) / (self.angle * math.pi)

def load_elem(elem):
    if elem['type'] == 'straight':
        dx = elem['delta']['x']
        dy = elem['delta']['y']
        dz = elem['delta']['z']
        return Straight(np.array([float(dx), float(dy), float(dz)]))
    elif elem['type'] == 'arc':
        dirx = elem['start_dir']['x']
        diry = elem['start_dir']['y']
        dirz = elem['start_dir']['z']
        cx = elem['center']['x']
        cy = elem['center']['y']
        cz = elem['center']['z']
        angle = elem['angle']
        return Arc(np.array([float(dirx), float(diry), float(dirz)]),
                   np.array([float(cx), float(cy), float(cz)]),
                   float(angle))
    else:
        print('Unknown type of element: {}'.format(elem['type']))
        return None

class Trajectory(object):
    ''' object to store trajectories to trace along by alphabets '''
    
    def __init__(self):
        self.start = np.array([0, 0, 0])
        self.elems = []
        self.elem_ends = []

    def load(self, file):
        ''' file contains start point of the trajectory and 
        sequence of linear elements (straight lines or curves) '''
        with open(file, 'r') as f:
            data = json.load(f)
        # erase old data
        self.elems = []
        # read new data
        start_x = float(data['start']['x'])
        start_y = float(data['start']['y'])
        start_z = float(data['start']['z'])
        self.start = np.array([start_x, start_y, start_z])
        for tr_elem in data['traj']:
            self.elems.append(load_elem(tr_elem))
        self._calc_elem_ends()
        self._gen_marker()

    def create_by_elements(self, start, elem_array):
        ''' create trajectory by elements '''
        self.start = start
        self.elems = elem_array
        self._calc_elem_ends()
        self._gen_marker()

    def __str__(self):
        ''' human-readable representation '''
        s = 'From {0:.3f},{1:.3f},{2:.3f}\n'.format(self.start[0],
                                                    self.start[1],
                                                    self.start[2])
        s += '  ' + '\n  '.join(str(x) for x in self.elems)
        return s

    def _calc_elem_ends(self):
        ''' trace all trajectory elements and 
        calculate endpoints for each part '''
        curr_p = self.start
        # drop previous values
        self.elem_ends = []
        # for each element of chain calculate
        # next position by previous
        for el in self.elems:
            curr_p = el.next_point(curr_p)
            self.elem_ends.append(curr_p)

    def _to_array(self):
        ''' convert trajectory to array of points (for to_marker_array method) '''
        ps = [self.start.copy()]
        for el in self.elems:
            new_ps = el.get_vis_points(ps[-1])
            ps.extend(new_ps)
        return ps

    def _gen_marker(self):
        self.marker = Marker()
        self.marker.header.frame_id = 'map'
        self.marker.action = Marker.ADD
        self.marker.lifetime = rospy.Duration(1000.0)
        self.marker.type = Marker.LINE_STRIP
        self.marker.scale.x = 0.005
        self.marker.scale.y = 0.005
        self.marker.scale.z = 0.005
        self.marker.pose.orientation.w = 1
    
    def to_marker_array(self, color):
        ''' convert trajectory to marker array for publication '''
        result_msg = MarkerArray()
        curr_time = rospy.Time.now()
        # create points of the trajectory
        ps = self._to_array()
        self.marker.id = 1
        self.marker.color.r = color[0]
        self.marker.color.g = color[1]
        self.marker.color.b = color[2]
        self.marker.color.a = color[3]
                
        self.marker.header.stamp = curr_time
        for p in ps:
            next_p = Point()
            next_p.x = p[0]*0.01
            next_p.y = p[1]*0.01
            next_p.z = p[2]*0.01
            self.marker.points.append(next_p)
        result_msg.markers = [self.marker]
        return result_msg


# list of printable symbols for movements
symbs = list(string.ascii_lowercase)
symbs.extend(list(string.ascii_uppercase))
    
class Alphabet(object):
    def __init__(self):
        self.letters = []

    def load(self, file):
        with open(file, 'r') as f:
            data = json.load(f)
        # erase old data
        self.letters = []
        for lt in data:
            self.letters.append(load_elem(lt))

    def __str__(self):
        s = ''
        for i,el in enumerate(self.letters):
            s += symbs[i] + ': ' + str(el)+'\n'
        return s

    def trace(self, traj, start_point, debug=False):
        ''' create description of the trajectory based on letters from alphabet '''
        result_string = ''
        result_elems = []
        curr_point_real = start_point.copy()
        max_num = 1000
        # for each part of the trajectory
        for ind, traj_elem in enumerate(traj.elems):
            traj_start_real = curr_point_real.copy()
            curr_point_traj = traj.start.copy() if ind == 0 else traj.elem_ends[ind-1]
            trace_param = -float("inf")
            # for each letter find next point
            #while True:
            while max_num > 0:
                if debug:
                    print('result:', result_string)
                    print('Traj_elem:', str(traj_elem))
                    print('Curr point real:', curr_point_real)
                    print('Curr point traj:', curr_point_traj)
                pdiff = curr_point_real - curr_point_traj
                if debug: print('Diff:', pdiff)
                next_ps = [l.next_point(pdiff)
                           for l in self.letters]
                if debug: print('Next ps: ', next_ps)
                # for each point find distance to the trajectory
                dists = [traj_elem.error(nxtp) for nxtp in next_ps]
                if debug: print('Dists:', dists)
                # find minimum distance to trajectory
                min_dist = float("inf")
                for i in range(len(dists)):
                    dist, param = dists[i]
                    if param > trace_param and dists[i][0] < min_dist:
                        min_dist = dist
                if debug: print('min_dist =', min_dist)
                # get all values with minimal dist
                trs = [t if d==min_dist else float("-inf") for d,t in dists]
                min_ind = trs.index(max(trs))
                # and choose maximum value of trace
                ts = []
                if debug: print('min_ind =', min_ind)
                if debug: print('param = ', dists[min_ind][1])
                # add another symbol to the result
                result_string += symbs[min_ind]
                result_elems.append(copy.copy(self.letters[min_ind]))
                # move to next point of the trajectory
                curr_point_real += next_ps[min_ind] - pdiff
                # if the trajectory is finished, stop processing
                if dists[min_ind][1] >= 1:
                    break
                trace_param = dists[min_ind][1]
                if debug: print('trace: ', trace_param)
                max_num -= 1
        if max_num == 0:
            raise ValueError('Internal error; sequence is too long.')
        # create trajectory from elements
        result_traj = Trajectory()
        result_traj.create_by_elements(start_point, result_elems)
        return (result_string, result_traj)

def main():
    time.sleep(5)
    rospy.init_node('traj_to_string')
    # load trajectory and alphabet
    traj_file = rospy.get_param("~traj_file")
    letters_file = rospy.get_param("~letters_file")

    dest_traj = Trajectory()
    dest_traj.load(traj_file)
    print(str(dest_traj))

    letters = Alphabet()
    letters.load(letters_file)

    # create publishers
    pub_src = rospy.Publisher("init_trajectory", MarkerArray, queue_size=100)
    pub_res = rospy.Publisher("res_trajectory", MarkerArray, queue_size=100)
    
#    a1 = Arc(np.array([2.0, 0.0, 0.0]),
#            np.array([0.0, -10.0, 0.0]),
#             90.0)
#    print(a1.trace_param(np.array([4.0, 2.0, 5.0])))
#    exit(0)
    
    # read number of strings to compute
    num_ex = int(rospy.get_param("~res_number"))
    # read error of start position
    std_dev = float(rospy.get_param("~std_dev"))
    for i in range(num_ex):
        start_point = dest_traj.start + np.random.normal(0, std_dev, 3)
        #start_point = dest_traj.start.copy()
        #print(start_point)
        res_str, res_traj = letters.trace(dest_traj, start_point)
        print(res_str)
        ma_res = res_traj.to_marker_array([0.0, np.random.random(), np.random.random(), 1.0])
        pub_res.publish(ma_res)
        #print(letters.trace(dest_traj, np.array([28.6, -1.5, 0.0])))
        #print(letters.trace(dest_traj, np.array([30.0, 0.0, 0.0])))

    # publish data
    ma_src = dest_traj.to_marker_array([1.0, 0, 0, 1.0])
    pub_src.publish(ma_src)
    
    rospy.spin()
    
if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
